//
//  PropertyManagerTests.swift
//  SampleCodeTests
//
//  Created by UmairAfzal on 24/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import XCTest
@testable import SampleCode

class PropertyManagerTests: XCTestCase {

    /*
     - variable sut is an abbreviation of System Under Test
     */
    
    var sut: PropertyManager!
    let testProperty = Property(title: "Berlin House", id: 1, price: 500, address: "RiverHood Woods", imageUrl: "https://somedummyImage", isFavorite: true)

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = PropertyManager()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: - Initial Value Test

    func testInit_PropertiesFavoriteCount_ReturnsZero() {
        XCTAssertEqual(sut.favoritePropertiesCount, 0)
    }

    // MARK: - Add Property As Favorite Test

    func testAdd_PropertyAsFavorite_ReturnsOne() {
        sut.addPropertyAsFavorite(property: testProperty)
        
        XCTAssertEqual(sut.favoritePropertiesCount, 1)
    }

    // MARK: - Avoid Duplication

    func testDuplicateProperty_ShouldNotBeAddedToArray() {
        sut.addPropertyAsFavorite(property: testProperty)
        sut.addPropertyAsFavorite(property: testProperty)

        XCTAssertEqual(sut.favoritePropertiesCount, 1)
    }
}
