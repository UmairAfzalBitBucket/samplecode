//
//  SampleCode_deTests.swift
//  SampleCodeTests
//
//  Created by UmairAfzal on 22/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import XCTest
@testable import SampleCode

class SampleCodeTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    // MARK: - Initialization Tests

    func testInit_PropertyModel() {
        let testProperty = Property(title: "Dummy Name", id: 1, price: 250, address: "Berlin Germany", imageUrl: "https://someImageUrl.com", isFavorite: false)
        XCTAssertNotNil(testProperty)
        XCTAssertEqual(testProperty.title, "Dummy Title")
        XCTAssertEqual(testProperty.id, 1)
        XCTAssertEqual(testProperty.price, 250)
        XCTAssertEqual(testProperty.address, "Berlin Germany")
        XCTAssertEqual(testProperty.imageUrl, "https://someImageUrl.com")
        XCTAssertEqual(testProperty.isFavorite, false)
    }

    // MARK: - Equatable Tests

    func testEquatable_ReturnsTrue() {
        let property1 = Property(title: "Test1", id: 1, price: 200, address: "Sosme dummy address", imageUrl: "somedummy url", isFavorite: false)
        let property2 = Property(title: "Test1", id: 1, price: 200, address: "Sosme dummy address", imageUrl: "somedummy url", isFavorite: false)
        XCTAssertEqual(property1, property2)
    }
}
