//
//  PropertyViewModel.swift
//  SampleCode
//
//  Created by UmairAfzal on 22/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class ViewModel: NSObject {

    // MARK :- IBOUtlets and variables

    /* This apiClient outlet is marked as an @IBOutlet so that we can instantiate it from the storyboard.  I mark this with a bang operator (!) since I know it will not be nil since the storyboard will be injecting it. */

    @IBOutlet var apiClient: APIClient!

    var isConnected = false
    var properties: [Property] = []
    var property: [SampleCodeProperty] = []
    var imageCache = NSCache<AnyObject, AnyObject>()

    let appDelegate = UIApplication.shared.delegate as? AppDelegate

    // MARK: - API Call Method

    func getProperties(completion: @escaping () -> Void) {

        if !Reachability.isConnectedToNetwork() {

            fetchProperties(completion: { (completed, propertiesReturned) in

                if completed {

                    for everyProperty in propertiesReturned! {
                        let property = Property(property: everyProperty)
                        self.properties.append(property)
                    }

                      /* Putting this block on the main queue because our completion handler is where the data display code will happen and we don't want to block any UI code. */
                    
                    DispatchQueue.main.async() {
                        completion()
                    }

                } else {

                      /* Putting this block on the main queue because our completion handler is where the data display code will happen and we don't want to block any UI code. */

                    DispatchQueue.main.async() {
                        completion()
                    }
                }
            })

        } else {

            apiClient.fetchPropertyList { (propertiesReturned) in

                /* Putting this block on the main queue because our completion handler is where the data display code will happen and we don't want to block any UI code. */

                DispatchQueue.main.async() { [weak self] in
                    
                    self?.properties = propertiesReturned!
                    completion()
                }
            }
        }
    }

    // MARK: - Data For Table View

    func numberOfItemsToDisplay(in section: Int) -> Int {
        return (properties.count) + (properties.count) / 3
    }

    func propertyTiltle(for indexPath: IndexPath) -> String {
        return (properties[(indexPath.row - indexPath.row / 3)].title)
    }

    func propertyPrice(for indexPath: IndexPath) -> Int {
        return (properties[(indexPath.row - indexPath.row / 3)].price)
    }

    func propertyAddress(for indexPath: IndexPath) -> String {
        return (properties[(indexPath.row - indexPath.row / 3)].address)
    }

    func propertyImageUrl(for indexPath: IndexPath, completion: @escaping (UIImage?) -> ()) {

        if let image = imageCache.object(forKey: (properties[(indexPath.row - indexPath.row / 3)].imageUrl as NSString?)!) as? UIImage {
            completion(image)

        } else {

            if let url = URL(string: (properties[(indexPath.row - indexPath.row / 3)].imageUrl)) {

                URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in

                    if error != nil {
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            completion(#imageLiteral(resourceName: "no_property"))
                        })

                    } else {
                        let image = UIImage(data: data!)
                        self?.imageCache.setObject(image ?? #imageLiteral(resourceName: "no_property"), forKey: (self?.properties[(indexPath.row - indexPath.row / 3)].imageUrl as NSString?)!)

                        DispatchQueue.main.async(execute: { () -> Void in
                            completion(image ?? #imageLiteral(resourceName: "no_property"))
                        })
                    }
                    }.resume()

            } else {
                completion(nil)
            }
        }
    }

    // MARK: - Private Methods

    func markAsFavorite(for indexPath: IndexPath) {
        properties[indexPath.row - indexPath.row / 3].isFavorite = true
    }

    func isPropertyFavorite(for indexPath: IndexPath) -> Bool {

        if properties[(indexPath.row - indexPath.row / 3)].isFavorite == true {
            return true
        }
        return false
    }

    // MARK: - CoreData Methods

    func savePropertyAsFavouriteInDB(for indexPath: IndexPath ,completion: (_ finsihed: Bool) -> ()) {

        guard let managedContext =  appDelegate?.persistentContainer.viewContext else { return }
        let property = SampleCodeProperty(context: managedContext)
        property.title = properties[indexPath.row - indexPath.row / 3].title
        property.price = Int32(properties[indexPath.row - indexPath.row / 3].price)
        property.address = properties[indexPath.row - indexPath.row / 3].address
        property.id = Int32(properties[indexPath.row - indexPath.row / 3].id)
        property.imageUrl = properties[indexPath.row - indexPath.row / 3].imageUrl
        property.isFavorite = true

        do {
            try managedContext.save()
            completion(true)

        } catch {
            debugPrint("Could not save: \(error.localizedDescription)")
            completion(false)
        }
    }


    func fetchProperties(completion: (_ finished: Bool, _ propertiesReturned: [SampleCodeProperty]?) -> ()) {

        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }

        let fetchRequest = NSFetchRequest<SampleCodeProperty>(entityName: "SampleCodeProperty")

        do {
            property = try managedContext.fetch(fetchRequest)

            if property.count > 0 {
                completion(true, property)

            } else {
                completion(false, [])
            }


        } catch {
            debugPrint("Could not fetch: \(error.localizedDescription)")
            completion(false, nil)
        }
    }
}
