//
//  Utility.swift
//  SampleCode
//
//  Created by UmairAfzal on 23/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import Foundation
import UIKit

class Utility : NSObject {

    class func emptyTableViewMessageWithImage(image: UIImage, message: String, viewBackgroundColor: UIColor = UIColor.white, viewController: UIViewController, tableView: UITableView) {
        let noJobsView = NoPropertiesView.instanceFromNib()
        noJobsView.imageView.image = image
        noJobsView.messageLabel.text = message
        noJobsView.messageLabel.font = UIFont.systemFont(ofSize: 15.0)
        noJobsView.backgroundColor = viewBackgroundColor
        tableView.backgroundView = noJobsView
        tableView.separatorStyle = .none
    }
}
