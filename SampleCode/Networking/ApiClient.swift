//
//  ApiClient.swift
//  SampleCode
//
//  Created by UmairAfzal on 22/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import UIKit
class APIClient: NSObject {

    // MARK: - Variables

    var propertiesArray =  [Property]()

    // MARK: - Private Methods

    /* The completion handler will be executed after properties data is fetched
     our completion handler will include an optional array of NSDictionaries parsed from our retrieved JSON object */

    func fetchPropertyList(completion: @escaping ([Property]?) -> Void) {

        // unwrap our API endpoint
        guard let url = URL(string: kBaseUrl) else { return }

        // create a session and dataTask on that session to get data/response/error
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { [weak self] (data, response, error) in

            // unwrap our returned data
            guard let unwrappedData = data else { return }

            do {

                if let responseJSON = try JSONSerialization.jsonObject(with: unwrappedData, options: .allowFragments) as? NSDictionary {

                    if let properties = responseJSON.value(forKeyPath: "items") as? [Dictionary<String, AnyObject>] {

                        for property in properties {
                            let property = Property(propertyDict: property)
                            self?.propertiesArray.append(property)
                        }
                        completion(self?.propertiesArray)
                    }
                }

            } catch {
                completion(nil)
                print("Error getting API data: \(error.localizedDescription)")
            }
        }
        dataTask.resume()
    }
}
