//
//  UIView.swift
//  SampleCode
//
//  Created by UmairAfzal on 23/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import UIKit

extension UIView {

    @IBInspectable
    var isCirculer: Bool {

        get {
            return layer.cornerRadius == min(self.frame.width, self.frame.height) / CGFloat(2.0) ? true : false
        }

        set {

            if newValue {
                layer.cornerRadius = self.frame.height/2
                self.clipsToBounds = true

            } else {
                layer.cornerRadius = 0.0
                self.clipsToBounds = false
            }
        }
    }

    @IBInspectable
    var cornerRadius: CGFloat {

        get {
            return layer.cornerRadius
        }

        set {
            layer.cornerRadius = newValue
        }
    }
}
