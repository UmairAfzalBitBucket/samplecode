//
//  SampleCodePropertyTableViewController.swift
//  SampleCode
//
//  Created by UmairAfzal on 22/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import UIKit
import Foundation

class SampleCodePropertyTableViewController: UITableViewController {

    // MARK: - IBOutlets And Variables

    @IBOutlet weak var viewModel: ViewModel!

    var isFirstLoad = true

    // MARK: - UIViewController Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        viewModel.getProperties {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.isFirstLoad = false
            self.tableView.reloadData()
        }
    }

    // MARK: - TableView DataSource & Delegate Methods

    override func numberOfSections(in tableView: UITableView) -> Int {

        if viewModel.numberOfItemsToDisplay(in: 0) != 0 {
            tableView.backgroundView = UIView()
            return 1
        }

        if !isFirstLoad {
            Utility.emptyTableViewMessageWithImage(image: #imageLiteral(resourceName: "no_property"), message: "No properties found", viewController: self, tableView: tableView)
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsToDisplay(in: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row != 0 && (indexPath.row + 1) %  3 == 0 {
            let cell = AdvertisementTableViewCell.cellForTableView(tableView: tableView, atIndexPath: indexPath)
            return cell
        }
        
        let cell = PropertyTableViewCell.cellForTableView(tableView: tableView, atIndexPath: indexPath)
        cell.delegate = self
        cell.propertyTitleLabel.text = viewModel.propertyTiltle(for: indexPath)
        cell.propertyPriceLabel.text = "€ \(viewModel.propertyPrice(for: indexPath))/-"
        cell.propertyAddressLabel.text = viewModel.propertyAddress(for: indexPath)

        if viewModel.isPropertyFavorite(for: indexPath) {
            cell.favoriteButton.setImage(#imageLiteral(resourceName: "btn_favorite_active"), for: .normal)

        } else {
            cell.favoriteButton.setImage(#imageLiteral(resourceName: "btn_favorite"), for: .normal)
        }

        viewModel.propertyImageUrl(for: indexPath) { image in

            if (image != nil) {
                cell.propertyImageView.image = image

            } else {
                cell.propertyImageView.image = #imageLiteral(resourceName: "no_property")
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        // Setting inital state
        cell.alpha = 0.4
        let transform = CATransform3DTranslate(CATransform3DIdentity, -tableView.bounds.size.width, 30, 0)
        cell.layer.transform = transform

        // Animating to final stage
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1.0
            cell.layer.transform = CATransform3DIdentity
        }
    }
}

extension SampleCodePropertyTableViewController: PropertyTableViewCellDelegate {

    // MARK: - PropertyTableViewCellDelegate
    
    func markedAsFavorite(cell: PropertyTableViewCell) {
        let indexPath = tableView.indexPath(for: cell)!
        viewModel.markAsFavorite(for: indexPath)

        viewModel.savePropertyAsFavouriteInDB(for: indexPath) { (completed) in

            if completed {
                print("Success: Property marked favorite successfully")
            }
        }
    }
}
