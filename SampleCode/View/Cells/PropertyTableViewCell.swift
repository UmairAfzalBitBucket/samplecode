//
//  PropertyTableViewCell.swift
//  SampleCode
//
//  Created by UmairAfzal on 23/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import UIKit

protocol PropertyTableViewCellDelegate {
    func markedAsFavorite(cell: PropertyTableViewCell)
}

class PropertyTableViewCell: UITableViewCell {

    // MARK: - IBOutlets And Variables
    
    @IBOutlet weak var propertyImageView: UIImageView!
    @IBOutlet weak var propertyTitleLabel: UILabel!
    @IBOutlet weak var propertyPriceLabel: UILabel!
    @IBOutlet weak var propertyAddressLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!

    var delegate: PropertyTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    class func cellForTableView(tableView: UITableView, atIndexPath indexPath: IndexPath) -> PropertyTableViewCell {
        let kPropertyTableViewCellIdentifier = "kPropertyTableViewCellIdentifier"
        tableView.register(UINib(nibName: "PropertyTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: kPropertyTableViewCellIdentifier)
        let cell = tableView.dequeueReusableCell(withIdentifier: kPropertyTableViewCellIdentifier, for: indexPath) as! PropertyTableViewCell
        return cell
    }

    // MARK: - IBActions

    @IBAction func likeButtonTapped(_ sender: UIButton) {

        if favoriteButton.currentImage == #imageLiteral(resourceName: "btn_favorite") {
            favoriteButton.setImage(#imageLiteral(resourceName: "btn_favorite_active"), for: .normal)
            delegate?.markedAsFavorite(cell: self)
        }
    }
}
