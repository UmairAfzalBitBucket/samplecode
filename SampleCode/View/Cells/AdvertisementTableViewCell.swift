//
//  AdvertisementTableViewCell.swift
//  SampleCode
//
//  Created by UmairAfzal on 23/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import UIKit

class AdvertisementTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    class func cellForTableView(tableView: UITableView, atIndexPath indexPath: IndexPath) -> AdvertisementTableViewCell {

        let kAdvertisementTableViewCellIdentifier = "kAdvertisementTableViewCellIdentifier"
        tableView.register(UINib(nibName: "AdvertisementTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: kAdvertisementTableViewCellIdentifier)
        let cell = tableView.dequeueReusableCell(withIdentifier: kAdvertisementTableViewCellIdentifier, for: indexPath) as! AdvertisementTableViewCell
        return cell
    }
}
