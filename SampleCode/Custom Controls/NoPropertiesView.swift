//
//  NoPropertiesView.swift
//  SampleCode
//
//  Created by UmairAfzal on 23/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import UIKit

class NoPropertiesView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!

    class func instanceFromNib() -> NoPropertiesView {
        return UINib(nibName: "NoPropertiesView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NoPropertiesView
    }
}
