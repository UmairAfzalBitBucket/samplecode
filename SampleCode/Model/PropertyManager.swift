//
//  PropertyManager.swift
//  SampleCode
//
//  Created by UmairAfzal on 24/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import Foundation

class PropertyManager {

    /*
     - This class is created to support Unit Tests keeping the future scope of usecases of this application.
     - However a few test cases are covered in this demo test.
     */

    // MARK: - IBOUtlets and variables

    var favoritePropertiesCount = 0
    private var propertiesToBeFavorite = [Property]()

    // MARK: - Private Methods
    
    func addPropertyAsFavorite(property: Property) {

        if !propertiesToBeFavorite.contains(property) {
            favoritePropertiesCount += 1
            propertiesToBeFavorite.append(property)
        }
    }
}
