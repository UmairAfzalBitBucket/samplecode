//
//  Property.swift
//  SampleCode
//
//  Created by UmairAfzal on 22/06/2018.
//  Copyright © 2018 UmairAfzal. All rights reserved.
//

import UIKit

class Property: Equatable {

     var id = 0
     var title = ""
     var price  = 0
     var address = ""
     var imageUrl = ""
     var isFavorite = false

    /*
     - This init method is to populate our Model Property from json incase of online view.
     */

    init(propertyDict: Dictionary<String, AnyObject>) {

        if let id = propertyDict["id"] as? Int {
            self.id = id
        }
        
        if let title = propertyDict["title"] as? String {
            self.title = title
        }

        if let price = propertyDict["price"] as? Int {
            self.price = price
        }

        if let location = propertyDict["location"] as? Dictionary<String, AnyObject> {

            if let address = location["address"] as? String {
                self.address = address
            }
        }

        if let images = propertyDict["images"] as? [Dictionary<String,AnyObject>] {

            if let url = images[0]["url"] as? String {
                self.imageUrl = url
            }
        }
    }

    /*
     - This init method is to populate our Model Property from CoreData incase of offline view.
     */

    init(property: SampleCodeProperty) {
        self.address = property.address!
        self.title = property.title!
        self.imageUrl = property.imageUrl!
        self.id = Int(property.id)
        self.price = Int(property.price)
        self.isFavorite = property.isFavorite
    }

    /*
     - This init method is to validate unit test for our Model Property
     */

    init(title: String, id: Int, price: Int, address: String, imageUrl: String, isFavorite: Bool) {
        self.title = title
        self.id = id
        self.price = price
        self.address = address
        self.imageUrl = imageUrl
        self.isFavorite = isFavorite
    }

    // MARK: - Equatable Protocol
    
    static func ==(lhs: Property, rhs: Property) -> Bool {

        if lhs.title == rhs.title {
            return true
        }
        return false
    }
}
